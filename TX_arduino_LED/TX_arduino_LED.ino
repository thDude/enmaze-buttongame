/*RF code
   SEIFEDDINE MEJRI -22/04/2017
   m2m MACHINEATTACK
   Transmitter code RF tx
*/
//LIBRARY
# include <VirtualWire.h>
#define TX_PIN 12
#define ON_PIN 6
#define OFF_PIN 7

int loopNr = 0;
String msg = "Message";
boolean IS_ON = false;

int btn1 = 0;
int btn2 = 0;

void setup() {
  Serial.begin(9600);
  pinMode(TX_PIN, OUTPUT);
  pinMode(ON_PIN, INPUT_PULLUP);
  pinMode(OFF_PIN, INPUT_PULLUP);

  //IO wires
  vw_set_tx_pin(TX_PIN);// TX pinout
  vw_setup(2000);
}

void loop() {
   btn1 = digitalRead(ON_PIN);
   btn2 = digitalRead(OFF_PIN);

    /*
   Serial.print("BTN 1 = ");
   Serial.print(btn1);
   Serial.print(";   ");
   Serial.print("BTN 2 = ");
   Serial.print(btn2);
   Serial.println();
    */
   if(btn1 == 0){
      IS_ON = true;
   }
   if(btn2 == 1){
      IS_ON = false;
   }

   Serial.print("IS_ON = ");
   Serial.print(IS_ON);
   Serial.println();

   if(IS_ON){
      sendString("1",true);
      digitalWrite(13,1);
   } else {
      sendString("0",true);
      digitalWrite(13,0);
   }

  delay(500);
}

void sendString(String message, bool wait) {
  byte messageLength = message.length() + 1;
  char charBuffer[messageLength];
  message.toCharArray(charBuffer, messageLength);

  vw_send((uint8_t *)charBuffer, messageLength);

  if (wait)vw_wait_tx();

  Serial.println("sent:" + message);



}


