#define PUSH_BUTTONS 6
#define START_LED 10
#define START_BTN 11

const int pushPin[] = {8, 9, A4, A5, 0, A0};
const int ledPin[] = { 2, 3, 4, 5, 6, 7};
int pressed[PUSH_BUTTONS];
int released[PUSH_BUTTONS];
int led_timer[PUSH_BUTTONS];

void setup() {
  Serial.begin(9600);
  for (int i = 0; i < PUSH_BUTTONS; i++) {
    pinMode(pushPin[i], INPUT_PULLUP);
    pressed[i] = false;
    released[i] = false;
  }
  pinMode(START_BTN , INPUT_PULLUP);
  for (int i = 0; i < PUSH_BUTTONS; i++) {
    pinMode(ledPin[i], OUTPUT);
  }
  pinMode(START_LED , OUTPUT);
}

void loop() {
  //readButtons();
  /*
  int btn_down = analogRead(A7) > 1 ? 1 : 0;
  Serial.println(btn_down);
  delay(100);
  */

  
  for (int i = 0; i < PUSH_BUTTONS; i++) {
    int btn_down = digitalRead(pushPin[i]);
    
    if (btn_down) {
      if (!pressed[i] && !released[i]) {
        pressed[i] = true;
      }
      if (!pressed[i] && released[i]) {
        pressed[i] = true;
        released[i] = false;
      }
    } else {
      if (pressed[i] && !released[i]) {
        released[i] = true;
        pressed[i] = false;
        Serial.print(" pressed  ");
        Serial.println(i);        
      }
    }
  }
  
}

void readButtons() {
  static int pressed[PUSH_BUTTONS];
  static int released[PUSH_BUTTONS];

  for (int i = 0; i < PUSH_BUTTONS; i++) {
    int btn_down = digitalRead(pushPin[i]);
    if (btn_down) {
      if (!pressed[i] && !released[i]) {
        pressed[i] = true;
      }
      if (!pressed[i] && released[i]) {
        pressed[i] = true;
        released[i] = false;
      }
    } else {
      if (pressed[i] && !released[i]) {
        released[i] = true;
        pressed[i] = false;
      }
    }
  }
}

