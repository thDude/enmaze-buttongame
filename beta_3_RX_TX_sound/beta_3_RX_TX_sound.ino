#include <FastLED.h>
#include <SoftwareSerial.h>
#include "Adafruit_Soundboard.h"

boolean DEBUG = true;

/*
   to do:
   use the TX TR communication with soundboard -> https://github.com/adafruit/Adafruit_Soundboard_library
*/

// Choose any two pins that can be used with SoftwareSerial to RX & TX
#define SFX_TX A1
#define SFX_RX A2

// Connect to the RST pin on the Sound Board
#define SFX_RST A3

// we'll be using software serial
SoftwareSerial ss = SoftwareSerial(SFX_TX, SFX_RX);
// pass the software serial to Adafruit_soundboard, the second
// argument is the debug port (not used really) and the third
// arg is the reset pin
Adafruit_Soundboard sfx = Adafruit_Soundboard(&ss, NULL, SFX_RST);
// can also try hardware serial with
// Adafruit_Soundboard sfx = Adafruit_Soundboard(&Serial1, NULL, SFX_RST);

/***  Hardware Setup ***/
#define LED_BAR_PIN   12
#define NUM_LEDS 14
#define BRIGHTNESS  200
CRGB leds[NUM_LEDS];

#define PUSH_BUTTONS 6
#define START_LED 10
#define START_BTN 11
const int pushPin[] = {8, 9, A4, A5, 0, A0};
const int ledPin[] = { 2, 3, 4, 5, 6, 7};
//const int soundPin[] = { A0, A1, A2, A3, A4, A5 };

int btn_pressed[PUSH_BUTTONS];
int btn_released[PUSH_BUTTONS];


/***  Game Variables *****/
#define max_sequence_length  32

//int playSound = 0;

int current_sequence_length = 12;
int current_correct_sequence[max_sequence_length];
int current_input_sequence[max_sequence_length];

int time_to_solve_sequence;
int time_led_is_shown;
int cur_position_in_sequence;
int cur_position_in_game;

int level = 0;
int maxLevel = 5;
int game_state = 0; // 0 = idle; 1 = start; 2 = ingame; 3 = level_finished; 4 = game over, 5 = finished;


/* Timer */
unsigned long timer[10];
byte timerState[10];
/* END Timer */


/*** colors ***/

const TProgmemPalette16 over PROGMEM =
{
  CRGB::Red,
  CRGB::Black,
  CRGB::Red,
  CRGB::Black,

  CRGB::Red,
  CRGB::Black,
  CRGB::Red,
  CRGB::Black,

  CRGB::Red,
  CRGB::Black,
  CRGB::Red,
  CRGB::Black,

  CRGB::Red,
  CRGB::Black,
  CRGB::Red,
  CRGB::Black
};

CRGBPalette16 gameOver = over;


void setup() {
  delay( 500 ); // power-up safety delay
  Serial.begin(9600);
  Serial.println("Adafruit Sound Board!");

  // softwareserial at 9600 baud
  ss.begin(9600);
  // can also do Serial1.begin(9600)

  FastLED.addLeds<NEOPIXEL, LED_BAR_PIN>(leds, 0 , 13);
  FastLED.setBrightness(  BRIGHTNESS );

  for (int i = 0; i < PUSH_BUTTONS; i++) {
    pinMode(pushPin[i], INPUT_PULLUP);
    btn_pressed[i] = false;
    btn_released[i] = false;
  }
  pinMode(START_BTN , INPUT_PULLUP);

  for (int i = 0; i < PUSH_BUTTONS; i++) {
    pinMode(ledPin[i], OUTPUT);
  }
  pinMode(START_LED , OUTPUT);

  //  for (int i = 0; i < 6; i++) {
  //    pinMode(soundPin[i], OUTPUT);
  //    digitalWrite(soundPin[i], HIGH);
  //  }



  game_state = 0;

  if (!sfx.reset()) {
    Serial.println("Not found");
    while (1);
  }
  Serial.println("SFX board found");
}

void loop() {
  switch (game_state) {
    case 0:     //idle
      idle();
      break;
    case 1:
      start_countdown_animation();
      break;
    case 2: //play the game
      switch (level) {
        case 0:
          game_sequence(1, 300, 3500, 6);
          break;
        case 1:
          game_sequence(2, 200, 1000, 12);
          break;
        case 2:
          game_sequence(3, 180, 700, 15);
          break;
        case 3:
          game_sequence(4, 150, 500, 16);
          break;
        case 4:
          game_sequence(5, 150, 500, 20);
          break;
      }
      break;
    case 3: //level done
      level_won();
      break;
    case 4: //game over
      game_over_animation();
      break;
    case 5: //finished
      game_won();
      break;
  }

  FastLED.show();
}




void idle() {
  if (level < maxLevel) {
    //start_countdown_animation();
  } else {
    game_state = 5;
  }
  
  digitalWrite( START_LED , HIGH);
  int val = digitalRead(START_BTN);

  if (!val) {
    digitalWrite( START_LED , LOW);
    game_state = 1;
  }
}

void game_over_animation() {
  //level = 0;
  static boolean showAnimation = true;

  static int gHue = 0;
  EVERY_N_MILLISECONDS( 1 ) {
    gHue++;
  }

  if (showAnimation) {
    for ( int i = 0; i < NUM_LEDS; i++) {
      leds[i] = ColorFromPalette(gameOver, gHue - (i));
    }
  } else {
    for (int i = 0; i < NUM_LEDS; i++ ) {
      leds[i].setRGB(0, 0, 0);
    }
  }

  if (delayMilliSeconds(3, 500)) {
    timerState[3] = 0;
    showAnimation = false;
  }


  digitalWrite( START_LED , HIGH);
  int val = digitalRead(START_BTN);

  if (!val) {
    digitalWrite( START_LED , LOW);
    game_state = 1;
    timerState[3] = 0;
    showAnimation = true;
  }
}

void level_won() {
  static bool first_frame = true;
  if (first_frame) {
    first_frame = false;
    playSound(2);
  }
  // FastLED's built-in rainbow generator
  static int gHue = 0;
  fill_rainbow( leds, NUM_LEDS, gHue, 7);
  EVERY_N_MILLISECONDS( 1 ) {
    gHue++;
  }
  if (delayMilliSeconds(3, 3000)) {
    for (int i = 0; i < NUM_LEDS; i++ ) {
      leds[i].setRGB(0, 0, 0);
    }
    timerState[3] = 0;
    level ++;
    game_state = 0;
    first_frame = true;
  }
}

void game_won() {
  static bool first_frame = true;
  if (first_frame) {
    first_frame = false;
    playSound(0);
  }
  // FastLED's built-in rainbow generator
  static int gHue = 0;
  fill_rainbow( leds, NUM_LEDS, gHue, 7);
  EVERY_N_MILLISECONDS( 10 ) {
    gHue++;
  }
  if (delayMilliSeconds(3, 15000)) {
    first_frame = true;
    timerState[3] = 0;
    level = 0;
    game_state = 0;
    for (int i = 0; i < NUM_LEDS; i++ ) {
      leds[i].setRGB(0, 0, 0);
    }

    //TODO: send RF signal to go trigger winning state 
  }
}

int delayMilliSeconds(int timerNumber, unsigned long delaytime) {
  unsigned long timeTaken;
  if (timerState[timerNumber] == 0) { //If the timer has been reset (which means timer (state ==0) then save millis() to the same number timer,
    timer[timerNumber] = millis();
    timerState[timerNumber] = 1;    //now we want mark this timer "not reset" so that next time through it doesn't get changed.
  }
  if (millis() > timer[timerNumber]) {
    timeTaken = millis() + 1 - timer[timerNumber]; //here we see how much time has passed
  }
  else {
    timeTaken = millis() + 2 + (4294967295 - timer[timerNumber]); //if the timer rolled over (more than 48 days passed)then this line accounts for that
  }
  if (timeTaken >= delaytime) {        //here we make it easy to wrap the code we want to time in an "IF" statement, if not then it isn't and so doesn't get run.
    timerState[timerNumber] = 0; //once enough time has passed the timer is marked reset.
    return 1;                          //if enough time has passed the "IF" statement is true
  }
  else {                               //if enough time has not passed then the "if" statement will not be true.
    return 0;
  }
}

void start_countdown_animation() {
  static int countdown_state = 0;
  static int countdown_interval = 400;
  static bool first_frame = true;

  switch (countdown_state) {
    case 0:
      for (int i = 0; i < NUM_LEDS; i++ ) {
        leds[i].setRGB(180, 50, 0);
      }
      if (first_frame) {
        first_frame = false;
        playSound(1);
      }
      break;
    case 1:
      for (int i = 0; i < NUM_LEDS; i++ ) {
        leds[i].setRGB(180, 50, 0);
      }
      break;
    case 2:
      for (int i = 0; i < NUM_LEDS; i++ ) {
        leds[i].setRGB(180, 50, 0);
      }
      break;
    case 3:
      for (int i = 0; i < NUM_LEDS; i++ ) {
        leds[i].setRGB(180, 110, 0);
      }

      break;
    case 4:
      for (int i = 0; i < NUM_LEDS; i++ ) {
        leds[i].setRGB(0, 255, 0);
      }
      break;
    case 5:
      countdown_state = 0;
      game_state = 2;
      first_frame = true;
      break;
  }

  EVERY_N_MILLISECONDS(countdown_interval) {
    //green should be longer
    if (countdown_state == 4) {
      delay(100);
    }

    for (int i = 0; i < NUM_LEDS; i++ ) {
      leds[i].setRGB(0, 0, 0);
    }
    FastLED.show();

    //green should be longer
    if (countdown_state != 4) {
      delay(80);
    }

    if (countdown_state == 3) {
      delay(50);
    }

    //first_frame = true;
    countdown_state++;
  }
}

void setup_sequence() {
  if (DEBUG) {
    Serial.print(" New Sequence: [ ");
  }
  for (int i = 0; i < current_sequence_length; i++ ) {
    current_correct_sequence[i] = random8(0, PUSH_BUTTONS);
    if (DEBUG) {
      if (i != 0)Serial.print(", ");
      Serial.print(current_correct_sequence[i]);
    }
  }
  if (DEBUG) {
    Serial.print("] ; ");
    Serial.println();
  }
}

void log() {
  for (int i = 0; i < PUSH_BUTTONS; i++) {
    int var = digitalRead(pushPin[i]);

    Serial.print(" Btn ");
    Serial.print(i);
    Serial.print(" = ");
    Serial.print(var);
    Serial.print(" ; ");
  }
  Serial.println(" ; ");
}

void game_sequence(int _btns_to_input, int _time_led_is_shown, int _time_to_solve_sequence, int _sequence_length ) {
  int btns_to_input = _btns_to_input;
  static int state = 0;
  static boolean init = true;

  static int cur_pos_in_sequence = 0;
  static int cur_pos_in_part = 0;
  static bool dead = false;

  static int pressed[PUSH_BUTTONS];
  static int released[PUSH_BUTTONS];

  time_to_solve_sequence = _time_to_solve_sequence;
  time_led_is_shown = _time_led_is_shown;
  current_sequence_length = _sequence_length;

  if (init) {
    setup_sequence();
    init = false;
  }


  switch (state) {
    case 0:     //show part of sequence
      for (int i = 0; i < btns_to_input; i++) {
        delay(10);
        digitalWrite( ledPin[current_correct_sequence[cur_pos_in_sequence + i]] , HIGH);
        if (DEBUG) {
          Serial.print(" show LED nr.  ");
          Serial.print(current_correct_sequence[cur_pos_in_sequence + i]);
          Serial.println();
        }
        delay(_time_led_is_shown);
        digitalWrite( ledPin[current_correct_sequence[cur_pos_in_sequence + i]] , LOW);
        if (DEBUG) {
          Serial.print(" hide LED nr.  ");
          Serial.print(current_correct_sequence[cur_pos_in_sequence + i]);
          Serial.println();
        }
        if (i != btns_to_input)delay(_time_led_is_shown);
      }
      timerState[0] = 0; //reset the timer
      state = 1;
      break;
    case 1:   //wait for input
      // start the timeout timer for this button
      if (delayMilliSeconds(0, _time_to_solve_sequence)) {
        dead = true;
        //int _playTune = random8(5,7);
        playSound(5);
        if (DEBUG) {
          Serial.print(" DIED timer run out!  ");
          Serial.println();
        }
      }

      for (int i = 0; i < PUSH_BUTTONS; i++) {
        int btn_down = digitalRead(pushPin[i]);
        if (btn_down) {
          if (!pressed[i] && !released[i]) {
            pressed[i] = true;
          }
          if (!pressed[i] && released[i]) {
            pressed[i] = true;
            released[i] = false;
          }
        } else {
          if (pressed[i] && !released[i]) {
            released[i] = true;
            pressed[i] = false;
            if (DEBUG) {
              Serial.print(" pressed  ");
              Serial.print(i);
              Serial.print("   Sequence Pos: ");
              Serial.print(cur_pos_in_sequence);
              Serial.print("    Part Pos: ");
              Serial.println(cur_pos_in_part);
            }

            //check if input is right
            if (i == current_correct_sequence[cur_pos_in_sequence + cur_pos_in_part]) {
              cur_pos_in_part++;
              timerState[0] = 0; //reset the timer
            } else {
              dead = true;
              //int _playTune = random8(4,5);
              playSound(4);
              if (DEBUG) {
                Serial.print(" WRONG BUTTON! pressed: ");
                Serial.print(i);
                Serial.print(", right one is: ");
                Serial.print(current_correct_sequence[cur_pos_in_sequence + cur_pos_in_part]);
                Serial.print("  ");
                Serial.println();
              }
            }
          }
        }
      }

      if (!dead && cur_pos_in_part == btns_to_input ) {
        cur_pos_in_sequence += cur_pos_in_part;
        cur_pos_in_part = 0;

        // if we finished the whole sequence
        if (cur_pos_in_sequence > current_sequence_length - 1) {
          delay(_time_led_is_shown);
          state = 2;
          timerState[0] = 0; //reset the timer
          //playSound = 3;
        } else { //or we still have some sequence left
          delay(_time_led_is_shown * 2);
          state = 0;
          timerState[0] = 0; //reset the timer
        }
      }

      //wrong button pushed?
      if (dead) {
        if (level > 0) {
          level--;
        }
        state = 3;
      }

      break;
    case 2:
      init = true;
      cur_pos_in_part = 0;
      cur_pos_in_sequence = 0;
      state = 0;
      dead = false;
      game_state = 3;
      if (DEBUG) {
        Serial.print(" Level ");
        Serial.print(level + 1);
        Serial.print(" Finished, moving on to next level!  ");
        Serial.println();
      }
      break;
    case 3:
      init = true;
      cur_pos_in_part = 0;
      cur_pos_in_sequence = 0;
      state = 0;
      game_state = 4;
      dead = false;
      for (int i = 0; i < PUSH_BUTTONS; i++) { //reset all LEDs
        digitalWrite( ledPin[i] , LOW);
        released[i] = false;
        pressed[i] = false;
      }
      if (DEBUG) {
        Serial.print(" DEAD! ");
        Serial.println();
      }
      return;

      break;
    case 4:

      break;
    case 5:

      break;
  }

}

void readButtons() {
  static int pressed[PUSH_BUTTONS];
  static int released[PUSH_BUTTONS];

  for (int i = 0; i < PUSH_BUTTONS; i++) {
    int btn_down = digitalRead(pushPin[i]);
    if (btn_down) {
      if (!pressed[i] && !released[i]) {
        pressed[i] = true;
      }
      if (!pressed[i] && released[i]) {
        pressed[i] = true;
        released[i] = false;
      }
    } else {
      if (pressed[i] && !released[i]) {
        released[i] = true;
        pressed[i] = false;
      }
    }
  }
}

void playSound(int _number) {
  //if (! sfx.stop() ) Serial.println("Failed to stop current track");
  if (! sfx.playTrack(_number) ) {
    Serial.print("track NR ");
    Serial.print(_number);
    Serial.println(" Failed to play");
  } else {
    Serial.print("track NR ");
    Serial.print(_number);
    Serial.println(" is playing");
  }
}

