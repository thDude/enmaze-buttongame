#include <FastLED.h>

#include "VirtualWire.h"
#define LED_BAR_PIN   2
#define NUM_LEDS 1
#define ON_PIN 6
#define OFF_PIN 7

byte messageLength = VW_MAX_MESSAGE_LEN; //the size of the message received
byte message[VW_MAX_MESSAGE_LEN];//a buffer to store the incoming message

boolean IS_ON = true;
CRGB leds[NUM_LEDS];
int j = 0;
void setup() {
  Serial.begin(9600);                  //arduino usb serial communication  bps
  Serial.println("device is ready...");

  FastLED.addLeds<NEOPIXEL, LED_BAR_PIN>(leds, 0 , 1);
  FastLED.setBrightness(  255 );
  pinMode(ON_PIN, INPUT_PULLUP);
  pinMode(OFF_PIN, INPUT_PULLUP);

  //virtual library RF setup
  vw_set_rx_pin(12);//RXpin
  vw_setup(2000); //bps
  //init virtual wire library rx part.
  vw_rx_start();
}

void loop() {
  digitalWrite(13,0);
  Serial.print("loop NR ");
  Serial.print(j);
  Serial.println(" started   ;");
  if (vw_get_message(message, &messageLength)) {
    digitalWrite(13,1);
    Serial.print("received: ");
    for (int i = 0; i < messageLength; i++) {
      Serial.write(message [i]);
    }
    if (message[0] == '1') {
      IS_ON = true;
    }
    if (message[0] == '0') {
      IS_ON = false;
    }

    Serial.println();
  }
  digitalWrite(13,0);
  if (IS_ON) {
    for (int i = 0; i < NUM_LEDS; i++ ) {
      leds[i].setRGB(0, 255, 0);
    }
  } else {
    for (int i = 0; i < NUM_LEDS; i++ ) {
      leds[i].setRGB(40, 40, 40);
    }
  }
  FastLED.show();
  delay(100);
  j++;
}
