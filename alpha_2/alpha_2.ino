#include <FastLED.h>


/***  Hardware Setup ***/
#define LED_BAR_PIN   12
#define NUM_LEDS 13
#define BRIGHTNESS  255
CRGB leds[NUM_LEDS];

#define PUSH_BUTTONS 4
#define START_LED 10
#define START_BTN 11
const int pushPin[] = {3, 5, 7 , 9};
const int ledPin[] = { 2, 4, 6, 8};



int push[PUSH_BUTTONS];

/***  Game Variables *****/
int current_correct_sequence[PUSH_BUTTONS];
int current_input_sequence[PUSH_BUTTONS];
int current_sequence_length;
int time_to_solve_sequence;
int cur_position_in_sequence;
int cur_position_in_game;

int level = 0;
int game_state = 0; // 0 = idle/game over; 1 = start; 2 = ingame; 3 = level_finished; 4 = game done;


void setup() {
  delay( 1000 ); // power-up safety delay
  Serial.begin(9600);

  FastLED.addLeds<NEOPIXEL, LED_BAR_PIN>(leds, 0 , 12);
  FastLED.setBrightness(  BRIGHTNESS );

  for (int i = 0; i < PUSH_BUTTONS; i++) {
    pinMode(pushPin[i], INPUT_PULLUP);
  }
  pinMode(START_BTN , INPUT_PULLUP);

  for (int i = 0; i < PUSH_BUTTONS; i++) {
    pinMode(ledPin[i], OUTPUT);
  }
  pinMode(START_LED , OUTPUT);


  game_state = 0;
}

void loop() {
  switch (game_state) {
    case 0:     //idle / game over
       idle();
      break;
    case 1:
      
      break;
    case 2:

      break;
    case 3:

      break;
  }
  for (int i = 0; i < PUSH_BUTTONS; i++) {
    push[i] = digitalRead(pushPin[i]);
    
//    Serial.print(" Btn ");
//    Serial.print(i);
//    Serial.print(" = ");
//    Serial.print(push[i]);
//    Serial.print(" ; ");

    if(push[i] == 1 ) {      
      digitalWrite( ledPin[i] , LOW);
    } else {
      digitalWrite( ledPin[i] , HIGH);
    }
  }
  Serial.println(" ; ");
}


void idle(){
  digitalWrite( START_LED , HIGH);
  int val = digitalRead(START_BTN);

  if(!val) {
    digitalWrite( START_LED , LOW);
    game_state = 1;
  }
}

