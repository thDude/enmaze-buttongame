#define PUSH_BUTTONS 4

const int pushPin[] = {3, 5, 7 , 9};
const int ledPin[] = { 2, 4, 6, 8};

int push[PUSH_BUTTONS];


void setup() {
  delay( 1000 ); // power-up safety delay
  Serial.begin(9600);

  for (int i = 0; i < PUSH_BUTTONS; i++) {
    pinMode(pushPin[i], INPUT_PULLUP);
  }

  for (int i = 0; i < PUSH_BUTTONS; i++) {
    pinMode(ledPin[i], OUTPUT);
  }
}

void loop() {
  for (int i = 0; i < PUSH_BUTTONS; i++) {
    push[i] = digitalRead(pushPin[i]);
    
    Serial.print(" Btn ");
    Serial.print(i);
    Serial.print(" = ");
    Serial.print(push[i]);
    Serial.print(" ; ");

    if(push[i] == 1 ) {      
      digitalWrite( ledPin[i] , LOW);
    } else {
      digitalWrite( ledPin[i] , HIGH);
    }
  }
  Serial.println(" ; ");
}
